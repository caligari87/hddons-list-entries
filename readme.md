## What?
This repository is just a place to store addon "entries" for [hddons-list](https://gitlab.com/dastrukar/hddons-list) to use that I'm working on.

- For info on how the website functions, please refer to the [readme.md of the website repository](https://gitlab.com/dastrukar/hddons-list/-/blob/master/readme.md).
- For a guide on how to add new addon entries, refer to [`contributing.md`](contributing.md).

This document will focus on trying to explain how the entries system works.

*(one day i will finally write proper documentation for this website)*


## Entry file name formatting
```
<ADDON_NAME> - <AUTHOR>
```

The file name doesn't have to exactly follow the name of the addon, but should at least be similar to the name of the addon.

It should be noted that all whitespace should be substituted with `_`.  
Though, you should use whitespace to seperate stuff.  
*(i suck at explaining, so just look at the files for an example)*


## Syntax for entries
All entries use YAML syntax and only have about 5 (or more?) variables to set.  
Please do note that all variable names are **case sensitive**.

I suggest that you use template.yaml as it's more convinient that way.

Note: It doesn't really matter as to how you order the variables, the only thing that matters is that you include them and set a value to them.

### Example
```YAML
title: "<Title/Name of the addon goes here>"
credits: "by <Author name here>"
flairs:
    - "<text>": "TYPE"
flag: "A class to set for this addon's entry goes here. Leave empty if none."
description: |
    This uses markdown syntax.
    
    Title
    <<<
    This is a dropdown
    <<<

    This is an iconlink|ICON|URLGOESHERE
```

### Minimalism
It should be noted that only `title`, `credits`, and `description` are required for the entry to display properly.

Which means you can have an entry that looks like this:
```JSON
title: "Title/Name"
credits: "by Author"
description: |
    This is a description that uses markdown syntax.
```

### Flags
- `pinned` : Puts the entry on the very top of the page, and gives it a nice green border.
- `bad` : Used for indicating that the addon is broken. Gives the entry a red border and a red title.

### Flair types
- `nice` : Blue colour. Used for anything that's not that important.
- `bad` : Red colour. Used for warnings and anything that's broken.
- `important` : Yellow colour. Used for stating requirements or anything that's important to note.

### Description syntax
One important thing to note about the description variable is that it uses Markdown syntax. (sort of)

Here are the following syntax that work in the description variable:
`````Markdown
description: |
    Please note the indentation.

    **bold**
    *italic*
    [link](url)
    `mono`
    ```
    block of mono
    ```
    
    > This is a quote

    ~smol text~
    ~~Strikethrough~~
    
    example(note the spacing):

    - list item 1
    - list item 2
    
    Title of dropdown
    <<<
    dropdown contents goes here
    
    - you can list stuff too!
    <<<

    This is an iconlink|ICON|URLGOESHERE
`````

## What's GitLab CI doing?
Every time a commit is pushed, GitLab CI will proceed to get all the JSON files in the given subdirectories (look at .gitlab-ci.yml) and put the contents into a JSON file named `<SUBDIRECTORY>_list.json`.

This list is then used by the [website](https://dastrukar.gitlab.io/hddons-list) to create addon entries.

Note: Lists are stored in a [seperate repository](https://gitlab.com/dastrukar/hddons-list-list) in order to keep the current repository's commit history more clean.
