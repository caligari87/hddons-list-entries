#!/usr/bin/env python3

# because i'm lazy to type a bunch of text to create files

import os

def get_input(prompt):
	print(prompt)
	return input()

# because these special characters are actually used for something else
def replace_unwanted_chars(text):
	text = text.replace(' ', '_')
	text = text.replace('/', '')
	text = text.replace('(', '')
	text = text.replace(')', '')
	text = text.replace('\\', '')
	text = text.replace("'", '')
	text = text.replace('"', '')
	text = text.replace('{', '')
	text = text.replace('}', '')
	text = text.replace('[', '')
	text = text.replace(']', '')
	text = text.replace('~', '')
	text = text.replace('`', '')
	text = text.replace('!', '')
	text = text.replace('@', '')
	text = text.replace('#', '')
	text = text.replace('$', '')
	text = text.replace('%', '')
	text = text.replace('^', '')
	text = text.replace(':', '')
	text = text.replace('&', "and")
	text = text.replace('*', '')
	return text


name = get_input("Name:")
cred = get_input("Credits:")
cate = get_input("Subdirectory:")

# Join the paths
target = os.path.join(cate, replace_unwanted_chars(name) + ' - ' + replace_unwanted_chars(cred) + '.yaml')

# Get template
with open("template.yaml", "r") as f:
	template = f.read()

# Set name and credits
lines = template.split("\n")
lines[0] = '{}"{}"'.format(lines[0][0:-2], name)
lines[1] = '{}"by {}"'.format(lines[1][0:-2], cred)

# Write the file
with open(target, "w") as f:
	f.write("\n".join(lines))
	print("Wrote to " + target)
