#!/usr/bin/env bash

regex=${1}
new=${2}

if [[ ${regex} == "" || ${new} == "" ]]
then
    echo "Usage: ${0} <regex> <replacement>"
    echo "Description: Replaces any regex matches with the provided replacement text for all entries."
    exit
fi

for i in *
do
    if [[ -d ${i} && ${i} != "list_output" ]]
    then
        for f in ${i}/*
        do
            sed -i "s/${regex}/${new}/g" "${f}"
        done
    fi
done
