## Prerequisites
- Python 3.X

## Rules

### Adding entries
- You may only add addons posted in the #mod-releases in the Hideous Destructor Discord Server. (might reconsider this)

### Removing entries
- Entries can only be removed if the author requests for it.

## Adding new entries
### Creating the YAML file
To create a new entry, run `new_entry.py` and fill in the details.   
After that it should automatically generate a new entry file for you.

If you don't want to use `new_entry.py`, you can just manually copy `template.yaml` and paste it into a subdirectory/category. (please make sure the filename uses the correct format)

### Filling in the rest
Once you've created the entry's YAML file, open up your preferred text editor and edit the file.

If you need help with the syntax, please refer to the `readme.md`.

Anyways, here's some notes:

- Variables are **case-sensitive**. Please make sure they are all in lowercase.
- Indent size is 4 spaces.
- Make sure to remove any unused variables!

`title` & `credits`:
- Self-explanatory.

`flairs`:
- Make sure to link any requirements.

`description`:
- Try to keep the description accurate to the original. You should only change it if required. (e.g: grammar errors, spelling errors, etc.)
- If there's no provided description, then just put "(no description provided)".
- Also, make sure the text is indented properly.

### Testing out your entry
If you're not sure if your new addon entry works, you can always just run `compile.py` and use the [preview page on the website.](https://dastrukar.gitlab.io/hddons-list/preview.html)

## Removing entries
To remove an entry, just delete the yaml file and it'll be automatically removed on the next CI pipeline.
